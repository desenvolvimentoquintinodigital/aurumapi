import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose";

@Schema({
    collection: "doc_situacao_pagamento"
})
export class SituacaoPagamentoModel {

    @Prop({ required: true })
    public descricao: string;

    @Prop({ required: true })
    public sigla: string;

}

export const SituacaoPagamentoSchema = SchemaFactory.createForClass(SituacaoPagamentoModel);
