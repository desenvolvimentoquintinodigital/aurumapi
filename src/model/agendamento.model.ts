import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose";

@Schema({
    collection: "doc_agendamento"
})
export class AgendamentoModel {

    @Prop({ required: true })
    public dataHoraNotificacao: Date;

    @Prop({ required: true })
    public tituloNotificacao: string;

    @Prop({ required: true })
    public mensagemNotificacao: string;

    @Prop({ required: true })
    public eRepetir: boolean;

}

export const AgendamentoSchema = SchemaFactory.createForClass(AgendamentoModel);