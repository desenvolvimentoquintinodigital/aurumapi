import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import * as mongoose from "mongoose";

@Schema({
    collection: "doc_arquivo"
})
export class ArquivoModel {

    public readonly _id: mongoose.Schema.Types.ObjectId;

    @Prop({ required: true })
    public nome: string;

    @Prop({ required: true })
    public extencao: string;

    @Prop({ required: true })
    public tamanho: number;

    @Prop({ required: true })
    public conteudo: Buffer;

    @Prop()
    public url: string;

}

export const ArquivoSchema = SchemaFactory.createForClass(ArquivoModel);