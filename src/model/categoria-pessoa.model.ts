import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema({
    collection: "doc_categoria_pessoa"
})
export class CategoriaPessoaModel {

    @Prop({ required: true, message: "Campo Obrigatório" })
    public descricao: string;

    @Prop({ required: true, message: "Campo Obrigatório" })
    public sigla: string;
}

export const CategoriaPessoaSchema = SchemaFactory.createForClass(CategoriaPessoaModel);