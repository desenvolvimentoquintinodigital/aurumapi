import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema({
    collection: "doc_categoria_lancamento_financeiro"
})
export class CategoriaLancamentoFinanceiroModel {

    @Prop({ required: true })
    public descricao: string;

}

export const CategoriaLancamentoFinanceiroSchema = SchemaFactory.createForClass(CategoriaLancamentoFinanceiroModel);
