import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose";

@Schema({
    collection: "doc_etiqueta"
})
export class EtiquetaModel {

    @Prop({ required: true, message: "Campo Obrigatório" })
    public nome: string;

    @Prop({ required: true, message: "Campo Obrigatório" })
    public cor: string;
}

export const EtiquetaSchema = SchemaFactory.createForClass(EtiquetaModel);