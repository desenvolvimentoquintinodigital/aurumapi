import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import * as mongoose from "mongoose";
import { CategoriaPessoaModel } from "./categoria-pessoa.model";

@Schema({
    collection: "doc_pessoa"
})
export class PessoaModel {

    @Prop({ required: true, message: "Campo Obrigatório" })
    public nome: string;

    @Prop({ required: true, default: true })
    public eAtivo: boolean;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: CategoriaPessoaModel.name })
    public categoriaPessoaID: CategoriaPessoaModel;

}

export const PessoaSchema = SchemaFactory.createForClass(PessoaModel);
