import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema({
    collection: "doc_categoria_forma_pagamento"
})
export class CategoriaFormaPagamentoModel {

    @Prop({ required: true })
    public descricao: string;

}

export const CategoriaFormaPagamentoSchema = SchemaFactory.createForClass(CategoriaFormaPagamentoModel);
