import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose";

@Schema({
    collection: "doc_localizacao"
})
export class LocalizacaoModel {

    @Prop({ required: true })
    public latitude: number;

    @Prop({ required: true })
    public longitude: number;

    @Prop({ required: true })
    public descricao: string;

}

export const LocalizacaoSchema = SchemaFactory.createForClass(LocalizacaoModel);