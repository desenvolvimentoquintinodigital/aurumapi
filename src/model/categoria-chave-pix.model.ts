import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema({
    collection: "doc_categoria_chave_pix"
})
export class CategoriaChavePixModel {

    @Prop({ required: true, unique: true })
    public descricao: string;

}

export const CategoriaChavePixSchema = SchemaFactory.createForClass(CategoriaChavePixModel);
