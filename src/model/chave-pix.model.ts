import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import * as mongoose from "mongoose";
import { CategoriaChavePixModel } from 'src/model/categoria-chave-pix.model';
import { PessoaModel } from './pessoa.model';

@Schema({
    collection: "doc_chave_pix"
})
export class ChavePixModel {

    @Prop({ required: true })
    public valorChave: string;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: PessoaModel.name })
    public pessoaID: PessoaModel = new PessoaModel();

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: CategoriaChavePixModel.name })
    public categoriaChavePixID: CategoriaChavePixModel = new CategoriaChavePixModel();

}

export const ChavePixSchema = SchemaFactory.createForClass(ChavePixModel);
