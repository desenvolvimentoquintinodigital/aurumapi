import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import * as mongoose from "mongoose";
import { CategoriaFormaPagamentoModel } from './categoria-forma-pagamento.model';
import { CategoriaLancamentoFinanceiroModel } from './categoria-lancamento-financeiro.model';
import { PessoaModel } from './pessoa.model';

@Schema({
    collection: "doc_lancamento_financeiro"
})
export class LancamentoFinanceiroModel {

    @Prop({ required: false })
    public identificadorLancamentoFinanceiro: string;

    @Prop({ required: true, default: Date.now() })
    public dataLancamentoFinanceiro: Date;

    @Prop()
    public dataVencimentoLancamento: Date;

    @Prop({ required: true })
    public dataPagamentoLancamento: Date;

    @Prop({ required: true })
    public valorTotalLancamento: number;

    @Prop({ required: true })
    public quantidadeParcelas: number;

    @Prop()
    public observacao: string;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: PessoaModel.name })
    public pessoaEstabelecimentoID: PessoaModel;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: CategoriaLancamentoFinanceiroModel.name })
    public categoriaLancamentoFinanceiroID: CategoriaLancamentoFinanceiroModel = new CategoriaLancamentoFinanceiroModel();

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: CategoriaLancamentoFinanceiroModel.name })
    public categoriaFormaPagamentoID: CategoriaFormaPagamentoModel = new CategoriaFormaPagamentoModel();

    // @Prop({ type: mongoose.Schema.Types.ObjectId, ref: SituacaoPagamentoModel.name })
    // public situacaoPagamentoModel: SituacaoPagamentoModel;

    // @Prop({ type: mongoose.Schema.Types.ObjectId, ref: ArquivoModel.name })
    // public arquivoModel: ArquivoModel;

    // @Prop({ type: mongoose.Schema.Types.ObjectId, ref: LocalizacaoModel.name })
    // public localizacaoModel: LocalizacaoModel;

    // @Prop({ type: mongoose.Schema.Types.ObjectId, ref: EtiquetaModel.name })
    // public etiquetaModel: EtiquetaModel;

    // @Prop({ type: mongoose.Schema.Types.ObjectId, ref: AgendamentoModel.name })
    // public agendamentoModel: AgendamentoModel;

}

export const LancamentoFinanceiroSchema = SchemaFactory.createForClass(LancamentoFinanceiroModel);
