import { Injectable } from '@nestjs/common';
import { table } from 'console';
import { ArquivoModel } from 'src/model/arquivo.model';
import { ArquivoRepository } from './../respository/arquivo.repository';

@Injectable()
export class ArquivoService {

    constructor(private arquivoRepository: ArquivoRepository) { }

    public async saveOne(file: Express.Multer.File) {
        const arquivo = await this.arquivoRepository.saveOne(this.recuperarArquivoModel(file));
        console.info(arquivo);
        return { message: "Arquivo salvo com sucesso!", urlArquivo: this.recuperarURL(arquivo) };
    }

    public async findAll() {
        return this.arquivoRepository.findAll();
    }

    public async findOne(arquivoID): Promise<Buffer> {
        return this.arquivoRepository.findOne(arquivoID);
    }

    private recuperarArquivoModel(file: Express.Multer.File) {
        const arquivoModel = new ArquivoModel();
            arquivoModel.nome = file.originalname;
            arquivoModel.extencao = this.recuperarExtencaoArquivo(file);
            arquivoModel.tamanho = file.size;
            arquivoModel.conteudo = file.buffer;
            arquivoModel.url = null;
        return arquivoModel;
    }

    private recuperarExtencaoArquivo(file: Express.Multer.File) : string {
        const extencaoList = file.mimetype.split("/", 2);
        return extencaoList[1].toUpperCase();
    }

    private recuperarURL(arquivoModel: ArquivoModel) {
        return `http://localhost:7070/arquivo/${arquivoModel._id}`;
    }

}
