import { Injectable } from '@nestjs/common';
import {PessoaRepository} from "../respository/pessoa.repository";
import {PessoaModel} from "../model/pessoa.model";

@Injectable()
export class PessoaService {

    constructor(private pessoaRepository: PessoaRepository) { }

    public async saveOne(pessoaModel: PessoaModel) {
        return this.pessoaRepository.saveOne(pessoaModel);
    }

    public async findAll() {
        return this.pessoaRepository.findAll();
    }

}
