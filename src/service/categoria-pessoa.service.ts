import {Injectable} from '@nestjs/common';
import {CategoriaPessoaModel} from "../model/categoria-pessoa.model";
import {CategoriaPessoaRepository} from "../respository/categoria-pessoa.repository";

@Injectable()
export class CategoriaPessoaService {

    constructor(private readonly categoriaPessoaRepository: CategoriaPessoaRepository) { }

    public async saveOne(categoriaPessoaModel: CategoriaPessoaModel) {
        return this.categoriaPessoaRepository.saveOne(categoriaPessoaModel);
    }

    public async findAll() {
        return await this.categoriaPessoaRepository.findAll();
    }

}
