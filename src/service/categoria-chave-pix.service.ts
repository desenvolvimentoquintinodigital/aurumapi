import { Injectable } from '@nestjs/common';
import { CategoriaChavePixModel } from 'src/model/categoria-chave-pix.model';
import { CategoriaChavePIXRepository } from './../respository/categoria-chave-pix.repository';

@Injectable()
export class CategoriaChavePIXService {

    constructor(private readonly categoriaChavePIXRepository: CategoriaChavePIXRepository) { }

    public async saveOne(categoriaChavePixModel: CategoriaChavePixModel) {
        return this.categoriaChavePIXRepository.saveOne(categoriaChavePixModel);
    }

    public async saveAll(categoriaChavePixModelList: CategoriaChavePixModel[]) {
        return this.categoriaChavePIXRepository.saveAll(categoriaChavePixModelList);
    }

    public async findAll() {
        return await this.categoriaChavePIXRepository.findAll();
    }

}
