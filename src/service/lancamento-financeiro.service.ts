import { Injectable } from '@nestjs/common';
import { LancamentoFinanceiroModel } from './../model/lancamento-financeiro.model';
import { LancamentoFinanceiroRepository } from './../respository/lancamento-financeiro.repository';

@Injectable()
export class LancamentoFinanceiroService {

    constructor(private readonly lancamentoFinanceiroRepository: LancamentoFinanceiroRepository) { }

    public async saveOne(lancamentoFinanceiroModel: LancamentoFinanceiroModel) {
        return this.lancamentoFinanceiroRepository.saveOne(lancamentoFinanceiroModel);
    }

    public async saveAll(lancamentoFinanceiroList: LancamentoFinanceiroModel[]) {
        return this.lancamentoFinanceiroRepository.saveAll(lancamentoFinanceiroList);
    }

    public async findAll() {
        return await this.lancamentoFinanceiroRepository.findAll();
    }

}
