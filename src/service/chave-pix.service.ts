import { Injectable } from '@nestjs/common';
import { ChavePixModel } from './../model/chave-pix.model';
import { ChavePIXRepository } from './../respository/chave-pix.repository';

@Injectable()
export class ChavePixService {

    constructor(private readonly chavePIXRepository: ChavePIXRepository) { }

    public async saveOne(chavePixModel: ChavePixModel) {
        return this.chavePIXRepository.saveOne(chavePixModel);
    }

    public async saveAll(chavePixModelList: ChavePixModel[]) {
        return this.chavePIXRepository.saveAll(chavePixModelList);
    }

    public async findAll() {
        return await this.chavePIXRepository.findAll();
    }

}
