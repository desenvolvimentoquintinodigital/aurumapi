import { CategoriaLancamentoFinanceiroModel } from './../model/categoria-lancamento-financeiro.model';
import { Injectable } from '@nestjs/common';
import { CategoriaPessoaModel } from "../model/categoria-pessoa.model";
import { CategoriaLancamentoFinanceiroRepository } from './../respository/categoria-lancamento-financeiro.repository';

@Injectable()
export class CategoriaLancamentoFinanceiroService {

    constructor(private readonly categoriaLancamentoFinanceiroRepository: CategoriaLancamentoFinanceiroRepository) { }

    public async saveOne(categoriaLancamentoFinanceiroModel: CategoriaLancamentoFinanceiroModel) {
        return this.categoriaLancamentoFinanceiroRepository.saveOne(categoriaLancamentoFinanceiroModel);
    }

    public async saveAll(categoriaLancamentoFinanceiroList: CategoriaLancamentoFinanceiroModel[]) {
        return this.categoriaLancamentoFinanceiroRepository.saveAll(categoriaLancamentoFinanceiroList);
    }

    public async findAll() {
        return await this.categoriaLancamentoFinanceiroRepository.findAll();
    }

}
