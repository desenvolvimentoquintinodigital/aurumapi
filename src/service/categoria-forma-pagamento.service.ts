import { Injectable } from '@nestjs/common';
import { CategoriaFormaPagamentoModel } from 'src/model/categoria-forma-pagamento.model';
import { CategoriaPessoaModel } from "../model/categoria-pessoa.model";
import { CategoriaFormaPagamentoRepository } from './../respository/categoria-forma-pagamento.repository';

@Injectable()
export class CategoriaFormaPagamentoService {

    constructor(private readonly categoriaFormaPagamentoRepository: CategoriaFormaPagamentoRepository) { }

    public async saveOne(categoriaFormaPagamentoModel: CategoriaFormaPagamentoModel) {
        return this.categoriaFormaPagamentoRepository.saveOne(categoriaFormaPagamentoModel);
    }

    public async findAll() {
        return await this.categoriaFormaPagamentoRepository.findAll();
    }

}
