import { Module } from '@nestjs/common';
import { MongooseModule } from "@nestjs/mongoose";
import { ArquivoController } from './../controller/arquivo.controller';
import { ArquivoModel, ArquivoSchema } from './../model/arquivo.model';
import { ArquivoRepository } from './../respository/arquivo.repository';
import { ArquivoService } from './../service/arquivo.service';

@Module({
    imports: [MongooseModule.forFeature([{name: ArquivoModel.name, schema: ArquivoSchema}])],
    controllers: [ArquivoController],
    providers: [
        ArquivoService,
        ArquivoRepository
    ]
})
export class ArquivoModule { }
