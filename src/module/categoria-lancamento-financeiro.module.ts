import { Module } from '@nestjs/common';
import { MongooseModule } from "@nestjs/mongoose";
import { CategoriaLancamentoFinanceiroController } from './../controller/categoria-lancamento-financeiro.controller';
import { CategoriaLancamentoFinanceiroModel, CategoriaLancamentoFinanceiroSchema } from './../model/categoria-lancamento-financeiro.model';
import { CategoriaLancamentoFinanceiroRepository } from './../respository/categoria-lancamento-financeiro.repository';
import { CategoriaLancamentoFinanceiroService } from './../service/categoria-lancamento-financeiro.service';

@Module({
    imports: [MongooseModule.forFeature([{name: CategoriaLancamentoFinanceiroModel.name, schema: CategoriaLancamentoFinanceiroSchema}])],
    controllers: [CategoriaLancamentoFinanceiroController],
    providers: [
        CategoriaLancamentoFinanceiroService,
        CategoriaLancamentoFinanceiroRepository
    ]
})
export class CategoriaLancamentoFinanceiroModule { }
