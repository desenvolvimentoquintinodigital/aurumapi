import { Module } from '@nestjs/common';
import { MongooseModule } from "@nestjs/mongoose";
import { LancamentoFinanceiroController } from './../controller/lancamento-financeiro.controller';
import { LancamentoFinanceiroModel, LancamentoFinanceiroSchema } from './../model/lancamento-financeiro.model';
import { LancamentoFinanceiroRepository } from './../respository/lancamento-financeiro.repository';
import { LancamentoFinanceiroService } from './../service/lancamento-financeiro.service';

@Module({
    imports: [MongooseModule.forFeature([{name: LancamentoFinanceiroModel.name, schema: LancamentoFinanceiroSchema}])],
    controllers: [LancamentoFinanceiroController],
    providers: [
        LancamentoFinanceiroService,
        LancamentoFinanceiroRepository
    ]
})
export class LancamentoFinanceiroModule { }
