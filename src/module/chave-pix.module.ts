import { Module } from '@nestjs/common';
import { MongooseModule } from "@nestjs/mongoose";
import { ChavePixController } from 'src/controller/chave-pix.controller';
import { ChavePixModel, ChavePixSchema } from './../model/chave-pix.model';
import { ChavePIXRepository } from './../respository/chave-pix.repository';
import { ChavePixService } from './../service/chave-pix.service';

@Module({
    imports: [MongooseModule.forFeature([{name: ChavePixModel.name, schema: ChavePixSchema}])],
    controllers: [ChavePixController],
    providers: [
        ChavePixService,
        ChavePIXRepository
    ]
})
export class ChavePixModule { }
