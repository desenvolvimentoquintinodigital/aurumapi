import {Module} from '@nestjs/common';
import {MongooseModule} from "@nestjs/mongoose";
import {PessoaModel, PessoaSchema} from "../model/pessoa.model";
import {PessoaController} from "../controller/pessoa.controller";
import {PessoaService} from "../service/pessoa.service";
import {PessoaRepository} from "../respository/pessoa.repository";

@Module({
    imports: [MongooseModule.forFeature([{name: PessoaModel.name, schema: PessoaSchema}])],
    controllers: [PessoaController],
    providers: [
        PessoaService,
        PessoaRepository
    ]
})
export class PessoaModule {}
