import { Module } from '@nestjs/common';
import { MongooseModule } from "@nestjs/mongoose";
import { CategoriaChavePixModel, CategoriaChavePixSchema } from 'src/model/categoria-chave-pix.model';
import { CategoriaChavePIXController } from '../controller/categoria-chave-pix.controller';
import { CategoriaChavePIXRepository } from './../respository/categoria-chave-pix.repository';
import { CategoriaChavePIXService } from './../service/categoria-chave-pix.service';

@Module({
    imports: [MongooseModule.forFeature([{name: CategoriaChavePixModel.name, schema: CategoriaChavePixSchema}])],
    controllers: [CategoriaChavePIXController],
    providers: [
        CategoriaChavePIXService,
        CategoriaChavePIXRepository
    ]
})
export class CategoriaChavePIXModule { }
