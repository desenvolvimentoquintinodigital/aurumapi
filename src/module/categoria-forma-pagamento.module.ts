import { Module } from '@nestjs/common';
import { MongooseModule } from "@nestjs/mongoose";
import { CategoriaFormaPagamentoSchema } from '../model/categoria-forma-pagamento.model';
import { CategoriaFormaPagamentoController } from './../controller/categoria-forma-pagamento.controller';
import { CategoriaFormaPagamentoModel } from './../model/categoria-forma-pagamento.model';
import { CategoriaFormaPagamentoRepository } from './../respository/categoria-forma-pagamento.repository';
import { CategoriaFormaPagamentoService } from './../service/categoria-forma-pagamento.service';

@Module({
    imports: [MongooseModule.forFeature([{name: CategoriaFormaPagamentoModel.name, schema: CategoriaFormaPagamentoSchema}])],
    controllers: [CategoriaFormaPagamentoController],
    providers: [
        CategoriaFormaPagamentoService,
        CategoriaFormaPagamentoRepository
    ]
})
export class CategoriaFormaPagamentoModule { }
