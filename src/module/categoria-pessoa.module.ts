import {Module} from '@nestjs/common';
import {MongooseModule} from "@nestjs/mongoose";
import {CategoriaPessoaModel, CategoriaPessoaSchema} from "../model/categoria-pessoa.model";
import {CategoriaPessoaController} from "../controller/categoria-pessoa.controller";
import {CategoriaPessoaService} from "../service/categoria-pessoa.service";
import {CategoriaPessoaRepository} from "../respository/categoria-pessoa.repository";

@Module({
    imports: [MongooseModule.forFeature([{name: CategoriaPessoaModel.name, schema: CategoriaPessoaSchema}])],
    controllers: [CategoriaPessoaController],
    providers: [
        CategoriaPessoaService,
        CategoriaPessoaRepository
    ]
})
export class CategoriaPessoaModule { }
