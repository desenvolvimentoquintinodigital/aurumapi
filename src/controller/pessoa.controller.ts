import {Body, Controller, Get, Post} from '@nestjs/common';
import {PessoaService} from "../service/pessoa.service";
import {PessoaModel} from "../model/pessoa.model";

@Controller('pessoa')
export class PessoaController {

    constructor(private readonly pessoaService: PessoaService) { }

    @Post()
    public async saveOne(@Body() pessoaModel: PessoaModel) {
        return this.pessoaService.saveOne(pessoaModel);
    }

    @Get()
    public async findAll() {
        return this.pessoaService.findAll();
    }

}
