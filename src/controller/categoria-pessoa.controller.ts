import {Body, Controller, Get, Post} from '@nestjs/common';
import {CategoriaPessoaService} from "../service/categoria-pessoa.service";
import {CategoriaPessoaModel} from "../model/categoria-pessoa.model";

@Controller('categoria-pessoa')
export class CategoriaPessoaController {

    constructor(private readonly categoriaPessoaService: CategoriaPessoaService) { }

    @Post()
    public async saveOne(@Body() categoriaPessoaModel: CategoriaPessoaModel) {
        return this.categoriaPessoaService.saveOne(categoriaPessoaModel);
    }

    @Get()
    public async findAll() {
        return await this.categoriaPessoaService.findAll();
    }

}
