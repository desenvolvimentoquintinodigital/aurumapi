import { Body, Controller, Get, Post } from '@nestjs/common';
import { LancamentoFinanceiroModel } from 'src/model/lancamento-financeiro.model';
import { LancamentoFinanceiroService } from './../service/lancamento-financeiro.service';

@Controller('lancamento-financeiro')
export class LancamentoFinanceiroController {

    constructor(private readonly lancamentoFinanceiroService: LancamentoFinanceiroService) { }

    @Post()
    public async saveOne(@Body() lancamentoFinanceiroModel: LancamentoFinanceiroModel) {
        console.log(lancamentoFinanceiroModel);        
        return this.lancamentoFinanceiroService.saveOne(lancamentoFinanceiroModel);
    }

    @Post("/all")
    public async saveAll(@Body() lancamentoFinanceiroModel: LancamentoFinanceiroModel[]) {
        return await this.lancamentoFinanceiroService.saveAll(lancamentoFinanceiroModel);
    }

    @Get()
    public async findAll() {
        return await this.lancamentoFinanceiroService.findAll();
    }

}
