import { Body, Controller, Get, Post } from '@nestjs/common';
import { CategoriaChavePixModel } from 'src/model/categoria-chave-pix.model';
import { CategoriaChavePIXService } from './../service/categoria-chave-pix.service';

@Controller('categoria-chave-pix')
export class CategoriaChavePIXController {

    constructor(private readonly categoriaChavePIXService: CategoriaChavePIXService) { }

    @Post()
    public async saveOne(@Body() categoriaChavePixModel: CategoriaChavePixModel) {
        console.log(categoriaChavePixModel);        
        return this.categoriaChavePIXService.saveOne(categoriaChavePixModel);
    }

    @Post("/all")
    public async saveAll(@Body() categoriaChavePixModel: CategoriaChavePixModel[]) {
        return await this.categoriaChavePIXService.saveAll(categoriaChavePixModel);
    }

    @Get()
    public async findAll() {
        return await this.categoriaChavePIXService.findAll();
    }

}
