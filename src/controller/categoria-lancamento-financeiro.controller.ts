import { Body, Controller, Get, Post } from '@nestjs/common';
import { CategoriaLancamentoFinanceiroModel } from './../model/categoria-lancamento-financeiro.model';
import { CategoriaLancamentoFinanceiroService } from './../service/categoria-lancamento-financeiro.service';

@Controller('categoria-lancamento-financeiro')
export class CategoriaLancamentoFinanceiroController {

    constructor(private readonly categoriaLancamentoFinanceiro: CategoriaLancamentoFinanceiroService) { }

    @Post()
    public async saveOne(@Body() categoriaLancamentoFinanceiroModel: CategoriaLancamentoFinanceiroModel) {
        return this.categoriaLancamentoFinanceiro.saveOne(categoriaLancamentoFinanceiroModel);
    }

    @Post("/all")
    public async saveAll(@Body() categoriaLancamentoFinanceiroList: CategoriaLancamentoFinanceiroModel[]) {
        return await this.categoriaLancamentoFinanceiro.saveAll(categoriaLancamentoFinanceiroList);
    }

    @Get()
    public async findAll() {
        return await this.categoriaLancamentoFinanceiro.findAll();
    }

}
