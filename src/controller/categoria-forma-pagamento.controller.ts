import { Body, Controller, Get, Post } from '@nestjs/common';
import { CategoriaFormaPagamentoModel } from 'src/model/categoria-forma-pagamento.model';
import { CategoriaFormaPagamentoService } from './../service/categoria-forma-pagamento.service';

@Controller('categoria-forma-pagamento')
export class CategoriaFormaPagamentoController {

    constructor(private readonly categoriaFormaPagamentoService: CategoriaFormaPagamentoService) { }

    @Post()
    public async saveOne(@Body() categoriaFormaPagamentoModel: CategoriaFormaPagamentoModel) {
        return this.categoriaFormaPagamentoService.saveOne(categoriaFormaPagamentoModel);
    }

    @Get()
    public async findAll() {
        return await this.categoriaFormaPagamentoService.findAll();
    }

}
