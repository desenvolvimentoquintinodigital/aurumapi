import { Body, Controller, Get, Post } from '@nestjs/common';
import { ChavePixModel } from './../model/chave-pix.model';
import { ChavePixService } from './../service/chave-pix.service';

@Controller('chave-pix')
export class ChavePixController {

    constructor(private readonly chavePixService: ChavePixService) { }

    @Post()
    public async saveOne(@Body() chavePixModel: ChavePixModel) {
        return this.chavePixService.saveOne(chavePixModel);
    }

    @Post("/all")
    public async saveAll(@Body() chavePixModel: ChavePixModel[]) {
        return await this.chavePixService.saveAll(chavePixModel);
    }

    @Get()
    public async findAll() {
        return await this.chavePixService.findAll();
    }

}
