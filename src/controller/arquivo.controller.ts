import { Controller, Get, Param, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ArquivoModel } from 'src/model/arquivo.model';
import { ArquivoService } from './../service/arquivo.service';

@Controller('arquivo')
export class ArquivoController {

    constructor(private readonly arquivoService: ArquivoService) { }

    @Post("upload")
    @UseInterceptors(FileInterceptor('file'))
    public async saveOne(@UploadedFile() file: Express.Multer.File) {
        return this.arquivoService.saveOne(file);
    }

    @Get()
    public async findAll() {
        return this.arquivoService.findAll();
    }

    @Get(":arquivoID")
    public async findOne(@Param("arquivoID") arquivoID): Promise<Buffer> {
        return this.arquivoService.findOne(arquivoID);
    }

}
