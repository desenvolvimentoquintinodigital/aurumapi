import { Module } from '@nestjs/common';
import { MongooseModule } from "@nestjs/mongoose";
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArquivoModule } from './module/arquivo.module';
import { CategoriaChavePIXModule } from './module/categoria-chave-pix.module';
import { CategoriaFormaPagamentoModule } from './module/categoria-forma-pagamento.module';
import { CategoriaLancamentoFinanceiroModule } from './module/categoria-lancamento-financeiro.module';
import { CategoriaPessoaModule } from "./module/categoria-pessoa.module";
import { ChavePixModule } from './module/chave-pix.module';
import { LancamentoFinanceiroModule } from './module/lancamento-financeiro.module';
import { PessoaModule } from './module/pessoa.module';

@Module({
  imports: [
    MongooseModule.forRoot("mongodb://localhost/aurum"),
      // MongooseModule.forRoot("mongodb+srv://josequintino:Kintino8@cluster0.rhiao.mongodb.net/aurum?retryWrites=true&w=majority"),
      CategoriaPessoaModule,
      PessoaModule,
      CategoriaLancamentoFinanceiroModule,
      LancamentoFinanceiroModule,
      CategoriaFormaPagamentoModule,
      CategoriaChavePIXModule,
      ChavePixModule,
      ArquivoModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
