import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CategoriaChavePixModel } from "src/model/categoria-chave-pix.model";

@Injectable()
export class CategoriaChavePIXRepository {

    constructor(
        @InjectModel(CategoriaChavePixModel.name) private readonly categoriaChavePIXRepository: Model<CategoriaChavePixModel>
    ) { }

    public async saveOne(categoriaChavePixModel: CategoriaChavePixModel) {
        return await new this.categoriaChavePIXRepository(categoriaChavePixModel).save();
    }

    public async saveAll(categoriaChavePixModelList: CategoriaChavePixModel[]) {
        for(let categoriaChavePIXModel of categoriaChavePixModelList) {
            new this.categoriaChavePIXRepository(categoriaChavePIXModel).save();
        }
        return categoriaChavePixModelList;
    }

    public async findAll() {
        return await this.categoriaChavePIXRepository.find().exec();
    }

}
