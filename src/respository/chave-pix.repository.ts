import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ChavePixModel } from './../model/chave-pix.model';

@Injectable()
export class ChavePIXRepository {

    constructor(
        @InjectModel(ChavePixModel.name) private readonly chavePixRepository: Model<ChavePixModel>
    ) { }

    public async saveOne(chavePixModel: ChavePixModel) {
        return await new this.chavePixRepository(chavePixModel).save();
    }

    public async saveAll(chavePixModelList: ChavePixModel[]) {
        for(let categoriaLancamentoFinanceiroModel of chavePixModelList) {
            new this.chavePixRepository(categoriaLancamentoFinanceiroModel).save();
        }
        return chavePixModelList;
    }

    public async findAll() {
        return await this.chavePixRepository.find().exec();
    }

}
