import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ArquivoModel } from 'src/model/arquivo.model';

@Injectable()
export class ArquivoRepository {

    constructor(@InjectModel(ArquivoModel.name) private readonly arquivoRepository: Model<ArquivoModel>) { }

    public async saveOne(arquivoModel: ArquivoModel) {
        return await new this.arquivoRepository(arquivoModel).save();
    }

    public async findAll() {
        return await this.arquivoRepository.find({}).lean().exec();
    }

    public async findOne(arquivoID): Promise<Buffer> {
        return (await this.arquivoRepository.findById(arquivoID)).conteudo;
    }

    public async deleteOne(arquivoID: string) {
        return await this.arquivoRepository.findByIdAndDelete(arquivoID);
    }

}
