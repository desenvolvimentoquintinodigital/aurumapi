import {CategoriaPessoaModel} from "../model/categoria-pessoa.model";
import {Model} from "mongoose";
import {Injectable} from "@nestjs/common";
import {InjectModel} from "@nestjs/mongoose";

@Injectable()
export class CategoriaPessoaRepository {

    constructor(
        @InjectModel(CategoriaPessoaModel.name) private readonly categoriaPessoaRepository: Model<CategoriaPessoaModel>
    ) { }

    public async saveOne(categoriaPessoaModel: CategoriaPessoaModel) {
        return await new this.categoriaPessoaRepository(categoriaPessoaModel).save();
    }

    public async findAll() {
        return await this.categoriaPessoaRepository.find().exec();
    }

}
