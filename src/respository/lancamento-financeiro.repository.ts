import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { LancamentoFinanceiroModel } from './../model/lancamento-financeiro.model';

@Injectable()
export class LancamentoFinanceiroRepository {

    constructor(
        @InjectModel(LancamentoFinanceiroModel.name) private readonly lancamentoFinanceiroRepository: Model<LancamentoFinanceiroModel>
    ) { }

    public async saveOne(lancamentoFinanceiroModel: LancamentoFinanceiroModel) {
        return await new this.lancamentoFinanceiroRepository(lancamentoFinanceiroModel).save();
    }

    public async saveAll(lancamentoFinanceiroList: LancamentoFinanceiroModel[]) {
        for(let lancamentoFinanceiroModel of lancamentoFinanceiroList) {
            new this.lancamentoFinanceiroRepository(lancamentoFinanceiroModel).save();
        }
        return lancamentoFinanceiroList;
    }

    public async findAll() {
        return await this.lancamentoFinanceiroRepository.find().exec();
    }

}
