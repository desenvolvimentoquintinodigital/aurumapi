import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CategoriaPessoaModel } from "../model/categoria-pessoa.model";
import { CategoriaFormaPagamentoModel } from './../model/categoria-forma-pagamento.model';

@Injectable()
export class CategoriaFormaPagamentoRepository {

    constructor(
        @InjectModel(CategoriaFormaPagamentoModel.name) private readonly formaPagamentoRepository: Model<CategoriaFormaPagamentoModel>
    ) { }

    public async saveOne(categoriaFormaPagamentoModel: CategoriaFormaPagamentoModel) {
        return await new this.formaPagamentoRepository(categoriaFormaPagamentoModel).save();
    }

    public async findAll() {
        return await this.formaPagamentoRepository.find().exec();
    }

}
