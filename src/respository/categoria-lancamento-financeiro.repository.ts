import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CategoriaLancamentoFinanceiroModel } from './../model/categoria-lancamento-financeiro.model';

@Injectable()
export class CategoriaLancamentoFinanceiroRepository {

    constructor(
        @InjectModel(CategoriaLancamentoFinanceiroModel.name) private readonly categoriaLancamentoFinanceiroRepository: Model<CategoriaLancamentoFinanceiroModel>
    ) { }

    public async saveOne(categoriaLancamentoFinanceiroModel: CategoriaLancamentoFinanceiroModel) {
        return await new this.categoriaLancamentoFinanceiroRepository(categoriaLancamentoFinanceiroModel).save();
    }

    public async saveAll(categoriaLancamentoFinanceiroList: CategoriaLancamentoFinanceiroModel[]) {
        for(let categoriaLancamentoFinanceiroModel of categoriaLancamentoFinanceiroList) {
            new this.categoriaLancamentoFinanceiroRepository(categoriaLancamentoFinanceiroModel).save();
        }
        return categoriaLancamentoFinanceiroList;
    }

    public async findAll() {
        return await this.categoriaLancamentoFinanceiroRepository.find().exec();
    }

}
