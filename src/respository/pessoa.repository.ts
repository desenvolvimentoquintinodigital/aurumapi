import {Injectable} from "@nestjs/common";
import {Model} from "mongoose";
import {PessoaModel} from "../model/pessoa.model";
import {InjectModel} from "@nestjs/mongoose";

@Injectable()
export class PessoaRepository {

    constructor(@InjectModel(PessoaModel.name) private readonly pessoaRespository: Model<PessoaModel>) { }

    public async saveOne(pessoaModel: PessoaModel) {
        return new this.pessoaRespository(pessoaModel).save();
    }

    public async findAll() {
        return this.pessoaRespository.find().populate("categoriaPessoaModel");
    }

}
