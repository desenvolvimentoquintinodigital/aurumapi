"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const arquivo_module_1 = require("./module/arquivo.module");
const categoria_chave_pix_module_1 = require("./module/categoria-chave-pix.module");
const categoria_forma_pagamento_module_1 = require("./module/categoria-forma-pagamento.module");
const categoria_lancamento_financeiro_module_1 = require("./module/categoria-lancamento-financeiro.module");
const categoria_pessoa_module_1 = require("./module/categoria-pessoa.module");
const chave_pix_module_1 = require("./module/chave-pix.module");
const lancamento_financeiro_module_1 = require("./module/lancamento-financeiro.module");
const pessoa_module_1 = require("./module/pessoa.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            mongoose_1.MongooseModule.forRoot("mongodb://localhost/aurum"),
            categoria_pessoa_module_1.CategoriaPessoaModule,
            pessoa_module_1.PessoaModule,
            categoria_lancamento_financeiro_module_1.CategoriaLancamentoFinanceiroModule,
            lancamento_financeiro_module_1.LancamentoFinanceiroModule,
            categoria_forma_pagamento_module_1.CategoriaFormaPagamentoModule,
            categoria_chave_pix_module_1.CategoriaChavePIXModule,
            chave_pix_module_1.ChavePixModule,
            arquivo_module_1.ArquivoModule
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map