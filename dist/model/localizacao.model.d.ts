/// <reference types="mongoose" />
export declare class LocalizacaoModel {
    latitude: number;
    longitude: number;
    descricao: string;
}
export declare const LocalizacaoSchema: import("mongoose").Schema<import("mongoose").Document<LocalizacaoModel, any, any>, import("mongoose").Model<import("mongoose").Document<LocalizacaoModel, any, any>, any, any, any>, any, any>;
