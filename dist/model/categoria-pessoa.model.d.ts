/// <reference types="mongoose" />
export declare class CategoriaPessoaModel {
    descricao: string;
    sigla: string;
}
export declare const CategoriaPessoaSchema: import("mongoose").Schema<import("mongoose").Document<CategoriaPessoaModel, any, any>, import("mongoose").Model<import("mongoose").Document<CategoriaPessoaModel, any, any>, any, any, any>, any, any>;
