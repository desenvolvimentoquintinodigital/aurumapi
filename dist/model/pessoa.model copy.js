"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PessoaSchema = exports.PessoaModel = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose = require("mongoose");
const categoria_pessoa_model_1 = require("./categoria-pessoa.model");
let PessoaModel = class PessoaModel {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true, message: "Campo Obrigatório" }),
    __metadata("design:type", String)
], PessoaModel.prototype, "nome", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true, default: true }),
    __metadata("design:type", Boolean)
], PessoaModel.prototype, "eAtivo", void 0);
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose.Schema.Types.ObjectId, ref: categoria_pessoa_model_1.CategoriaPessoaModel.name }),
    __metadata("design:type", categoria_pessoa_model_1.CategoriaPessoaModel)
], PessoaModel.prototype, "categoriaPessoaModel", void 0);
PessoaModel = __decorate([
    (0, mongoose_1.Schema)({
        collection: "doc_pessoa"
    })
], PessoaModel);
exports.PessoaModel = PessoaModel;
exports.PessoaSchema = mongoose_1.SchemaFactory.createForClass(PessoaModel);
//# sourceMappingURL=pessoa.model%20copy.js.map