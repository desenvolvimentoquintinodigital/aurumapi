"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChavePixSchema = exports.ChavePixModel = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose = require("mongoose");
const categoria_chave_pix_model_1 = require("./categoria-chave-pix.model");
const pessoa_model_1 = require("./pessoa.model");
let ChavePixModel = class ChavePixModel {
    constructor() {
        this.pessoaID = new pessoa_model_1.PessoaModel();
        this.categoriaChavePixID = new categoria_chave_pix_model_1.CategoriaChavePixModel();
    }
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ChavePixModel.prototype, "valorChave", void 0);
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose.Schema.Types.ObjectId, ref: pessoa_model_1.PessoaModel.name }),
    __metadata("design:type", pessoa_model_1.PessoaModel)
], ChavePixModel.prototype, "pessoaID", void 0);
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose.Schema.Types.ObjectId, ref: categoria_chave_pix_model_1.CategoriaChavePixModel.name }),
    __metadata("design:type", categoria_chave_pix_model_1.CategoriaChavePixModel)
], ChavePixModel.prototype, "categoriaChavePixID", void 0);
ChavePixModel = __decorate([
    (0, mongoose_1.Schema)({
        collection: "doc_chave_pix"
    })
], ChavePixModel);
exports.ChavePixModel = ChavePixModel;
exports.ChavePixSchema = mongoose_1.SchemaFactory.createForClass(ChavePixModel);
//# sourceMappingURL=chave-pix.model.js.map