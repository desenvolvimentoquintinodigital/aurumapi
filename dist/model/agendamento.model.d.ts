/// <reference types="mongoose" />
export declare class AgendamentoModel {
    dataHoraNotificacao: Date;
    tituloNotificacao: string;
    mensagemNotificacao: string;
    eRepetir: boolean;
}
export declare const AgendamentoSchema: import("mongoose").Schema<import("mongoose").Document<AgendamentoModel, any, any>, import("mongoose").Model<import("mongoose").Document<AgendamentoModel, any, any>, any, any, any>, any, any>;
