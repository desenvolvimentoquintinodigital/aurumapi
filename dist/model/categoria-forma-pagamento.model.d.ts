/// <reference types="mongoose" />
export declare class CategoriaFormaPagamentoModel {
    descricao: string;
}
export declare const CategoriaFormaPagamentoSchema: import("mongoose").Schema<import("mongoose").Document<CategoriaFormaPagamentoModel, any, any>, import("mongoose").Model<import("mongoose").Document<CategoriaFormaPagamentoModel, any, any>, any, any, any>, any, any>;
