"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EtiquetaSchema = exports.EtiquetaModel = void 0;
const mongoose_1 = require("@nestjs/mongoose");
let EtiquetaModel = class EtiquetaModel {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true, message: "Campo Obrigatório" }),
    __metadata("design:type", String)
], EtiquetaModel.prototype, "nome", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true, message: "Campo Obrigatório" }),
    __metadata("design:type", String)
], EtiquetaModel.prototype, "cor", void 0);
EtiquetaModel = __decorate([
    (0, mongoose_1.Schema)({
        collection: "doc_etiqueta"
    })
], EtiquetaModel);
exports.EtiquetaModel = EtiquetaModel;
exports.EtiquetaSchema = mongoose_1.SchemaFactory.createForClass(EtiquetaModel);
//# sourceMappingURL=etiqueta.model.js.map