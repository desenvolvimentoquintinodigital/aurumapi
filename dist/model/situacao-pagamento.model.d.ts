/// <reference types="mongoose" />
export declare class SituacaoPagamentoModel {
    descricao: string;
    sigla: string;
}
export declare const SituacaoPagamentoSchema: import("mongoose").Schema<import("mongoose").Document<SituacaoPagamentoModel, any, any>, import("mongoose").Model<import("mongoose").Document<SituacaoPagamentoModel, any, any>, any, any, any>, any, any>;
