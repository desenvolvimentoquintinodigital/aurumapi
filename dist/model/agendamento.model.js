"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AgendamentoSchema = exports.AgendamentoModel = void 0;
const mongoose_1 = require("@nestjs/mongoose");
let AgendamentoModel = class AgendamentoModel {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Date)
], AgendamentoModel.prototype, "dataHoraNotificacao", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], AgendamentoModel.prototype, "tituloNotificacao", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], AgendamentoModel.prototype, "mensagemNotificacao", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Boolean)
], AgendamentoModel.prototype, "eRepetir", void 0);
AgendamentoModel = __decorate([
    (0, mongoose_1.Schema)({
        collection: "doc_agendamento"
    })
], AgendamentoModel);
exports.AgendamentoModel = AgendamentoModel;
exports.AgendamentoSchema = mongoose_1.SchemaFactory.createForClass(AgendamentoModel);
//# sourceMappingURL=agendamento.model.js.map