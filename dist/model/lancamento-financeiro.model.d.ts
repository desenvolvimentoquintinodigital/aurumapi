import * as mongoose from "mongoose";
import { CategoriaFormaPagamentoModel } from './categoria-forma-pagamento.model';
import { CategoriaLancamentoFinanceiroModel } from './categoria-lancamento-financeiro.model';
import { PessoaModel } from './pessoa.model';
export declare class LancamentoFinanceiroModel {
    identificadorLancamentoFinanceiro: string;
    dataLancamentoFinanceiro: Date;
    dataVencimentoLancamento: Date;
    dataPagamentoLancamento: Date;
    valorTotalLancamento: number;
    quantidadeParcelas: number;
    observacao: string;
    pessoaEstabelecimentoID: PessoaModel;
    categoriaLancamentoFinanceiroID: CategoriaLancamentoFinanceiroModel;
    categoriaFormaPagamentoID: CategoriaFormaPagamentoModel;
}
export declare const LancamentoFinanceiroSchema: mongoose.Schema<mongoose.Document<LancamentoFinanceiroModel, any, any>, mongoose.Model<mongoose.Document<LancamentoFinanceiroModel, any, any>, any, any, any>, any, any>;
