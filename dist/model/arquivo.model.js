"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArquivoSchema = exports.ArquivoModel = void 0;
const mongoose_1 = require("@nestjs/mongoose");
let ArquivoModel = class ArquivoModel {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ArquivoModel.prototype, "nome", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ArquivoModel.prototype, "extencao", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Number)
], ArquivoModel.prototype, "tamanho", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Buffer)
], ArquivoModel.prototype, "conteudo", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], ArquivoModel.prototype, "url", void 0);
ArquivoModel = __decorate([
    (0, mongoose_1.Schema)({
        collection: "doc_arquivo"
    })
], ArquivoModel);
exports.ArquivoModel = ArquivoModel;
exports.ArquivoSchema = mongoose_1.SchemaFactory.createForClass(ArquivoModel);
//# sourceMappingURL=arquivo.model.js.map