import * as mongoose from "mongoose";
import { CategoriaPessoaModel } from "./categoria-pessoa.model";
export declare class PessoaModel {
    nome: string;
    eAtivo: boolean;
    categoriaPessoaModel: CategoriaPessoaModel;
}
export declare const PessoaSchema: mongoose.Schema<mongoose.Document<PessoaModel, any, any>, mongoose.Model<mongoose.Document<PessoaModel, any, any>, any, any, any>, any, any>;
