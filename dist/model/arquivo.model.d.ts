/// <reference types="node" />
import * as mongoose from "mongoose";
export declare class ArquivoModel {
    readonly _id: mongoose.Schema.Types.ObjectId;
    nome: string;
    extencao: string;
    tamanho: number;
    conteudo: Buffer;
    url: string;
}
export declare const ArquivoSchema: mongoose.Schema<mongoose.Document<ArquivoModel, any, any>, mongoose.Model<mongoose.Document<ArquivoModel, any, any>, any, any, any>, any, any>;
