"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoriaPessoaSchema = exports.CategoriaPessoaModel = void 0;
const mongoose_1 = require("@nestjs/mongoose");
let CategoriaPessoaModel = class CategoriaPessoaModel {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true, message: "Campo Obrigatório" }),
    __metadata("design:type", String)
], CategoriaPessoaModel.prototype, "descricao", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true, message: "Campo Obrigatório" }),
    __metadata("design:type", String)
], CategoriaPessoaModel.prototype, "sigla", void 0);
CategoriaPessoaModel = __decorate([
    (0, mongoose_1.Schema)({
        collection: "doc_categoria_pessoa"
    })
], CategoriaPessoaModel);
exports.CategoriaPessoaModel = CategoriaPessoaModel;
exports.CategoriaPessoaSchema = mongoose_1.SchemaFactory.createForClass(CategoriaPessoaModel);
//# sourceMappingURL=categoria-pessoa.model.js.map