import * as mongoose from "mongoose";
import { CategoriaChavePixModel } from 'src/model/categoria-chave-pix.model';
import { PessoaModel } from './pessoa.model';
export declare class ChavePixModel {
    valorChave: string;
    pessoaID: PessoaModel;
    categoriaChavePixID: CategoriaChavePixModel;
}
export declare const ChavePixSchema: mongoose.Schema<mongoose.Document<ChavePixModel, any, any>, mongoose.Model<mongoose.Document<ChavePixModel, any, any>, any, any, any>, any, any>;
