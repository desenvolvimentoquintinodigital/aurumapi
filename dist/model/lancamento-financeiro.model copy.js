"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LancamentoFinanceiroSchema = exports.LancamentoFinanceiroModel = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose = require("mongoose");
const categoria_forma_pagamento_model_1 = require("./categoria-forma-pagamento.model");
const categoria_lancamento_financeiro_model_1 = require("./categoria-lancamento-financeiro.model");
const pessoa_model_1 = require("./pessoa.model");
let LancamentoFinanceiroModel = class LancamentoFinanceiroModel {
    constructor() {
        this.categoriaLancamentoFinanceiroID = new categoria_lancamento_financeiro_model_1.CategoriaLancamentoFinanceiroModel();
        this.categoriaFormaPagamentoID = new categoria_forma_pagamento_model_1.CategoriaFormaPagamentoModel();
    }
};
__decorate([
    (0, mongoose_1.Prop)({ required: false }),
    __metadata("design:type", String)
], LancamentoFinanceiroModel.prototype, "identificadorLancamentoFinanceiro", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true, default: Date.now() }),
    __metadata("design:type", Date)
], LancamentoFinanceiroModel.prototype, "dataLancamentoFinanceiro", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", Date)
], LancamentoFinanceiroModel.prototype, "dataVencimentoLancamento", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Date)
], LancamentoFinanceiroModel.prototype, "dataPagamentoLancamento", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Number)
], LancamentoFinanceiroModel.prototype, "valorTotalLancamento", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Number)
], LancamentoFinanceiroModel.prototype, "quantidadeParcelas", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], LancamentoFinanceiroModel.prototype, "observacao", void 0);
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose.Schema.Types.ObjectId, ref: pessoa_model_1.PessoaModel.name }),
    __metadata("design:type", pessoa_model_1.PessoaModel)
], LancamentoFinanceiroModel.prototype, "pessoaEstabelecimentoID", void 0);
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose.Schema.Types.ObjectId, ref: categoria_lancamento_financeiro_model_1.CategoriaLancamentoFinanceiroModel.name }),
    __metadata("design:type", categoria_lancamento_financeiro_model_1.CategoriaLancamentoFinanceiroModel)
], LancamentoFinanceiroModel.prototype, "categoriaLancamentoFinanceiroID", void 0);
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose.Schema.Types.ObjectId, ref: categoria_lancamento_financeiro_model_1.CategoriaLancamentoFinanceiroModel.name }),
    __metadata("design:type", categoria_forma_pagamento_model_1.CategoriaFormaPagamentoModel)
], LancamentoFinanceiroModel.prototype, "categoriaFormaPagamentoID", void 0);
LancamentoFinanceiroModel = __decorate([
    (0, mongoose_1.Schema)({
        collection: "doc_lancamento_financeiro"
    })
], LancamentoFinanceiroModel);
exports.LancamentoFinanceiroModel = LancamentoFinanceiroModel;
exports.LancamentoFinanceiroSchema = mongoose_1.SchemaFactory.createForClass(LancamentoFinanceiroModel);
//# sourceMappingURL=lancamento-financeiro.model%20copy.js.map