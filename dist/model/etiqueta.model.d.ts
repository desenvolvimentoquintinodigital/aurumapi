/// <reference types="mongoose" />
export declare class EtiquetaModel {
    nome: string;
    cor: string;
}
export declare const EtiquetaSchema: import("mongoose").Schema<import("mongoose").Document<EtiquetaModel, any, any>, import("mongoose").Model<import("mongoose").Document<EtiquetaModel, any, any>, any, any, any>, any, any>;
