/// <reference types="mongoose" />
import { CategoriaLancamentoFinanceiroModel } from './../model/categoria-lancamento-financeiro.model';
import { CategoriaLancamentoFinanceiroRepository } from './../respository/categoria-lancamento-financeiro.repository';
export declare class CategoriaLancamentoFinanceiroService {
    private readonly categoriaLancamentoFinanceiroRepository;
    constructor(categoriaLancamentoFinanceiroRepository: CategoriaLancamentoFinanceiroRepository);
    saveOne(categoriaLancamentoFinanceiroModel: CategoriaLancamentoFinanceiroModel): Promise<import("mongoose").Document<any, any, CategoriaLancamentoFinanceiroModel> & CategoriaLancamentoFinanceiroModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    saveAll(categoriaLancamentoFinanceiroList: CategoriaLancamentoFinanceiroModel[]): Promise<CategoriaLancamentoFinanceiroModel[]>;
    findAll(): Promise<(import("mongoose").Document<any, any, CategoriaLancamentoFinanceiroModel> & CategoriaLancamentoFinanceiroModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
