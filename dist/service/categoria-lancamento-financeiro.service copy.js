"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoriaLancamentoFinanceiroService = void 0;
const common_1 = require("@nestjs/common");
const categoria_lancamento_financeiro_repository_1 = require("./../respository/categoria-lancamento-financeiro.repository");
let CategoriaLancamentoFinanceiroService = class CategoriaLancamentoFinanceiroService {
    constructor(categoriaLancamentoFinanceiroRepository) {
        this.categoriaLancamentoFinanceiroRepository = categoriaLancamentoFinanceiroRepository;
    }
    async saveOne(categoriaLancamentoFinanceiroModel) {
        return this.categoriaLancamentoFinanceiroRepository.saveOne(categoriaLancamentoFinanceiroModel);
    }
    async saveAll(categoriaLancamentoFinanceiroList) {
        return this.categoriaLancamentoFinanceiroRepository.saveAll(categoriaLancamentoFinanceiroList);
    }
    async findAll() {
        return await this.categoriaLancamentoFinanceiroRepository.findAll();
    }
};
CategoriaLancamentoFinanceiroService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [categoria_lancamento_financeiro_repository_1.CategoriaLancamentoFinanceiroRepository])
], CategoriaLancamentoFinanceiroService);
exports.CategoriaLancamentoFinanceiroService = CategoriaLancamentoFinanceiroService;
//# sourceMappingURL=categoria-lancamento-financeiro.service%20copy.js.map