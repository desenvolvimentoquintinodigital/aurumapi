"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LancamentoFinanceiroService = void 0;
const common_1 = require("@nestjs/common");
const lancamento_financeiro_repository_1 = require("./../respository/lancamento-financeiro.repository");
let LancamentoFinanceiroService = class LancamentoFinanceiroService {
    constructor(lancamentoFinanceiroRepository) {
        this.lancamentoFinanceiroRepository = lancamentoFinanceiroRepository;
    }
    async saveOne(lancamentoFinanceiroModel) {
        return this.lancamentoFinanceiroRepository.saveOne(lancamentoFinanceiroModel);
    }
    async saveAll(lancamentoFinanceiroList) {
        return this.lancamentoFinanceiroRepository.saveAll(lancamentoFinanceiroList);
    }
    async findAll() {
        return await this.lancamentoFinanceiroRepository.findAll();
    }
};
LancamentoFinanceiroService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [lancamento_financeiro_repository_1.LancamentoFinanceiroRepository])
], LancamentoFinanceiroService);
exports.LancamentoFinanceiroService = LancamentoFinanceiroService;
//# sourceMappingURL=lancamento-financeiro.service.js.map