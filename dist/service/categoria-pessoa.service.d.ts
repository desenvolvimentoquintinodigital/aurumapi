/// <reference types="mongoose" />
import { CategoriaPessoaModel } from "../model/categoria-pessoa.model";
import { CategoriaPessoaRepository } from "../respository/categoria-pessoa.repository";
export declare class CategoriaPessoaService {
    private readonly categoriaPessoaRepository;
    constructor(categoriaPessoaRepository: CategoriaPessoaRepository);
    saveOne(categoriaPessoaModel: CategoriaPessoaModel): Promise<import("mongoose").Document<any, any, CategoriaPessoaModel> & CategoriaPessoaModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    findAll(): Promise<(import("mongoose").Document<any, any, CategoriaPessoaModel> & CategoriaPessoaModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
