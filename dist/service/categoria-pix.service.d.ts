import { CategoriaPIXModel } from 'src/model/categoria-pix.model';
import { CategoriaPIXRepository } from '../respository/categoria-chave-pix.repository';
export declare class CategoriaPIXService {
    private readonly categoriaPIXRepository;
    constructor(categoriaPIXRepository: CategoriaPIXRepository);
    saveOne(categoriaPIXModel: CategoriaPIXModel): Promise<any>;
    saveAll(categoriaPIXModel: CategoriaPIXModel[]): Promise<any>;
    findAll(): Promise<any>;
}
