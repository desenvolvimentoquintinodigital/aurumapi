"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoriaPIXService = void 0;
const common_1 = require("@nestjs/common");
const categoria_chave_pix_repository_1 = require("../respository/categoria-chave-pix.repository");
let CategoriaPIXService = class CategoriaPIXService {
    constructor(categoriaPIXRepository) {
        this.categoriaPIXRepository = categoriaPIXRepository;
    }
    async saveOne(categoriaPIXModel) {
        return this.categoriaPIXRepository.saveOne(categoriaPIXModel);
    }
    async saveAll(categoriaPIXModel) {
        return this.categoriaPIXRepository.saveAll(categoriaPIXModel);
    }
    async findAll() {
        return await this.categoriaPIXRepository.findAll();
    }
};
CategoriaPIXService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [typeof (_a = typeof categoria_chave_pix_repository_1.CategoriaPIXRepository !== "undefined" && categoria_chave_pix_repository_1.CategoriaPIXRepository) === "function" ? _a : Object])
], CategoriaPIXService);
exports.CategoriaPIXService = CategoriaPIXService;
//# sourceMappingURL=categoria-pix.service.js.map