/// <reference types="mongoose" />
import { CategoriaChavePixModel } from 'src/model/categoria-chave-pix.model';
import { CategoriaChavePIXRepository } from './../respository/categoria-chave-pix.repository';
export declare class CategoriaChavePIXService {
    private readonly categoriaChavePIXRepository;
    constructor(categoriaChavePIXRepository: CategoriaChavePIXRepository);
    saveOne(categoriaChavePixModel: CategoriaChavePixModel): Promise<import("mongoose").Document<any, any, CategoriaChavePixModel> & CategoriaChavePixModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    saveAll(categoriaChavePixModelList: CategoriaChavePixModel[]): Promise<CategoriaChavePixModel[]>;
    findAll(): Promise<(import("mongoose").Document<any, any, CategoriaChavePixModel> & CategoriaChavePixModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
