/// <reference types="mongoose" />
import { CategoriaFormaPagamentoModel } from 'src/model/categoria-forma-pagamento.model';
import { CategoriaFormaPagamentoRepository } from './../respository/categoria-forma-pagamento.repository';
export declare class CategoriaFormaPagamentoService {
    private readonly categoriaFormaPagamentoRepository;
    constructor(categoriaFormaPagamentoRepository: CategoriaFormaPagamentoRepository);
    saveOne(categoriaFormaPagamentoModel: CategoriaFormaPagamentoModel): Promise<import("mongoose").Document<any, any, CategoriaFormaPagamentoModel> & CategoriaFormaPagamentoModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    findAll(): Promise<(import("mongoose").Document<any, any, CategoriaFormaPagamentoModel> & CategoriaFormaPagamentoModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
