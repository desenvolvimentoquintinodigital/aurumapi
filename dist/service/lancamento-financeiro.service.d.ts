/// <reference types="mongoose" />
import { LancamentoFinanceiroModel } from './../model/lancamento-financeiro.model';
import { LancamentoFinanceiroRepository } from './../respository/lancamento-financeiro.repository';
export declare class LancamentoFinanceiroService {
    private readonly lancamentoFinanceiroRepository;
    constructor(lancamentoFinanceiroRepository: LancamentoFinanceiroRepository);
    saveOne(lancamentoFinanceiroModel: LancamentoFinanceiroModel): Promise<import("mongoose").Document<any, any, LancamentoFinanceiroModel> & LancamentoFinanceiroModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    saveAll(lancamentoFinanceiroList: LancamentoFinanceiroModel[]): Promise<LancamentoFinanceiroModel[]>;
    findAll(): Promise<(import("mongoose").Document<any, any, LancamentoFinanceiroModel> & LancamentoFinanceiroModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
