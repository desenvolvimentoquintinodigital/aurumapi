/// <reference types="multer" />
/// <reference types="node" />
import { ArquivoModel } from 'src/model/arquivo.model';
import { ArquivoRepository } from './../respository/arquivo.repository';
export declare class ArquivoService {
    private arquivoRepository;
    constructor(arquivoRepository: ArquivoRepository);
    saveOne(file: Express.Multer.File): Promise<{
        message: string;
        urlArquivo: string;
    }>;
    findAll(): Promise<(ArquivoModel & {
        _id: import("mongoose").Schema.Types.ObjectId;
    })[]>;
    findOne(arquivoID: any): Promise<Buffer>;
    private recuperarArquivoModel;
    private recuperarExtencaoArquivo;
    private recuperarURL;
}
