/// <reference types="mongoose" />
import { ChavePixModel } from './../model/chave-pix.model';
import { ChavePIXRepository } from './../respository/chave-pix.repository';
export declare class ChavePixService {
    private readonly chavePIXRepository;
    constructor(chavePIXRepository: ChavePIXRepository);
    saveOne(chavePixModel: ChavePixModel): Promise<import("mongoose").Document<any, any, ChavePixModel> & ChavePixModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    saveAll(chavePixModelList: ChavePixModel[]): Promise<ChavePixModel[]>;
    findAll(): Promise<(import("mongoose").Document<any, any, ChavePixModel> & ChavePixModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
