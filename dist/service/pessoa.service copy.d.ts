/// <reference types="mongoose" />
import { PessoaRepository } from "../respository/pessoa.repository";
import { PessoaModel } from "../model/pessoa.model";
export declare class PessoaService {
    private pessoaRepository;
    constructor(pessoaRepository: PessoaRepository);
    saveOne(pessoaModel: PessoaModel): Promise<import("mongoose").Document<any, any, PessoaModel> & PessoaModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    findAll(): Promise<(import("mongoose").Document<any, any, PessoaModel> & PessoaModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
