"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoriaFormaPagamentoService = void 0;
const common_1 = require("@nestjs/common");
const categoria_forma_pagamento_repository_1 = require("./../respository/categoria-forma-pagamento.repository");
let CategoriaFormaPagamentoService = class CategoriaFormaPagamentoService {
    constructor(categoriaFormaPagamentoRepository) {
        this.categoriaFormaPagamentoRepository = categoriaFormaPagamentoRepository;
    }
    async saveOne(categoriaFormaPagamentoModel) {
        return this.categoriaFormaPagamentoRepository.saveOne(categoriaFormaPagamentoModel);
    }
    async findAll() {
        return await this.categoriaFormaPagamentoRepository.findAll();
    }
};
CategoriaFormaPagamentoService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [categoria_forma_pagamento_repository_1.CategoriaFormaPagamentoRepository])
], CategoriaFormaPagamentoService);
exports.CategoriaFormaPagamentoService = CategoriaFormaPagamentoService;
//# sourceMappingURL=categoria-forma-pagamento.service.js.map