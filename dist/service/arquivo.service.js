"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArquivoService = void 0;
const common_1 = require("@nestjs/common");
const arquivo_model_1 = require("../model/arquivo.model");
const arquivo_repository_1 = require("./../respository/arquivo.repository");
let ArquivoService = class ArquivoService {
    constructor(arquivoRepository) {
        this.arquivoRepository = arquivoRepository;
    }
    async saveOne(file) {
        const arquivo = await this.arquivoRepository.saveOne(this.recuperarArquivoModel(file));
        console.info(arquivo);
        return { message: "Arquivo salvo com sucesso!", urlArquivo: this.recuperarURL(arquivo) };
    }
    async findAll() {
        return this.arquivoRepository.findAll();
    }
    async findOne(arquivoID) {
        return this.arquivoRepository.findOne(arquivoID);
    }
    recuperarArquivoModel(file) {
        const arquivoModel = new arquivo_model_1.ArquivoModel();
        arquivoModel.nome = file.originalname;
        arquivoModel.extencao = this.recuperarExtencaoArquivo(file);
        arquivoModel.tamanho = file.size;
        arquivoModel.conteudo = file.buffer;
        arquivoModel.url = null;
        return arquivoModel;
    }
    recuperarExtencaoArquivo(file) {
        const extencaoList = file.mimetype.split("/", 2);
        return extencaoList[1].toUpperCase();
    }
    recuperarURL(arquivoModel) {
        return `http://localhost:7070/arquivo/${arquivoModel._id}`;
    }
};
ArquivoService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [arquivo_repository_1.ArquivoRepository])
], ArquivoService);
exports.ArquivoService = ArquivoService;
//# sourceMappingURL=arquivo.service.js.map