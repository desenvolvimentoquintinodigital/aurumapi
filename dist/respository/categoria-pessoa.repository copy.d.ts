import { CategoriaPessoaModel } from "../model/categoria-pessoa.model";
import { Model } from "mongoose";
export declare class CategoriaPessoaRepository {
    private readonly categoriaPessoaRepository;
    constructor(categoriaPessoaRepository: Model<CategoriaPessoaModel>);
    saveOne(categoriaPessoaModel: CategoriaPessoaModel): Promise<import("mongoose").Document<any, any, CategoriaPessoaModel> & CategoriaPessoaModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    findAll(): Promise<(import("mongoose").Document<any, any, CategoriaPessoaModel> & CategoriaPessoaModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
