import { Model } from "mongoose";
import { CategoriaPIXModel } from './../model/categoria-pix.model';
export declare class CategoriaPIXRepository {
    private readonly categoriaPIXRepository;
    constructor(categoriaPIXRepository: Model<CategoriaPIXModel>);
    saveOne(categoriaPIXModel: CategoriaPIXModel): Promise<any>;
    saveAll(categoriaPIXList: CategoriaPIXModel[]): Promise<CategoriaPIXModel[]>;
    findAll(): Promise<any[]>;
}
