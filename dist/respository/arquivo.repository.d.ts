/// <reference types="node" />
import { Model } from "mongoose";
import { ArquivoModel } from 'src/model/arquivo.model';
export declare class ArquivoRepository {
    private readonly arquivoRepository;
    constructor(arquivoRepository: Model<ArquivoModel>);
    saveOne(arquivoModel: ArquivoModel): Promise<import("mongoose").Document<any, any, ArquivoModel> & ArquivoModel & {
        _id: import("mongoose").Schema.Types.ObjectId;
    }>;
    findAll(): Promise<(ArquivoModel & {
        _id: import("mongoose").Schema.Types.ObjectId;
    })[]>;
    findOne(arquivoID: any): Promise<Buffer>;
    deleteOne(arquivoID: string): Promise<import("mongoose").Document<any, any, ArquivoModel> & ArquivoModel & {
        _id: import("mongoose").Schema.Types.ObjectId;
    }>;
}
