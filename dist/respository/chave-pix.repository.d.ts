import { Model } from "mongoose";
import { ChavePixModel } from './../model/chave-pix.model';
export declare class ChavePIXRepository {
    private readonly chavePixRepository;
    constructor(chavePixRepository: Model<ChavePixModel>);
    saveOne(chavePixModel: ChavePixModel): Promise<import("mongoose").Document<any, any, ChavePixModel> & ChavePixModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    saveAll(chavePixModelList: ChavePixModel[]): Promise<ChavePixModel[]>;
    findAll(): Promise<(import("mongoose").Document<any, any, ChavePixModel> & ChavePixModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
