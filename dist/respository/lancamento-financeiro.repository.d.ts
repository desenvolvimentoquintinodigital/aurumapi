import { Model } from "mongoose";
import { LancamentoFinanceiroModel } from './../model/lancamento-financeiro.model';
export declare class LancamentoFinanceiroRepository {
    private readonly lancamentoFinanceiroRepository;
    constructor(lancamentoFinanceiroRepository: Model<LancamentoFinanceiroModel>);
    saveOne(lancamentoFinanceiroModel: LancamentoFinanceiroModel): Promise<import("mongoose").Document<any, any, LancamentoFinanceiroModel> & LancamentoFinanceiroModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    saveAll(lancamentoFinanceiroList: LancamentoFinanceiroModel[]): Promise<LancamentoFinanceiroModel[]>;
    findAll(): Promise<(import("mongoose").Document<any, any, LancamentoFinanceiroModel> & LancamentoFinanceiroModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
