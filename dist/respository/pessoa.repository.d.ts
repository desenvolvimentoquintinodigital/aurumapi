import { Model } from "mongoose";
import { PessoaModel } from "../model/pessoa.model";
export declare class PessoaRepository {
    private readonly pessoaRespository;
    constructor(pessoaRespository: Model<PessoaModel>);
    saveOne(pessoaModel: PessoaModel): Promise<import("mongoose").Document<any, any, PessoaModel> & PessoaModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    findAll(): Promise<(import("mongoose").Document<any, any, PessoaModel> & PessoaModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
