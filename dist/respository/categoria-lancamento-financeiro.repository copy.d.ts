import { Model } from "mongoose";
import { CategoriaLancamentoFinanceiroModel } from './../model/categoria-lancamento-financeiro.model';
export declare class CategoriaLancamentoFinanceiroRepository {
    private readonly categoriaLancamentoFinanceiroRepository;
    constructor(categoriaLancamentoFinanceiroRepository: Model<CategoriaLancamentoFinanceiroModel>);
    saveOne(categoriaLancamentoFinanceiroModel: CategoriaLancamentoFinanceiroModel): Promise<import("mongoose").Document<any, any, CategoriaLancamentoFinanceiroModel> & CategoriaLancamentoFinanceiroModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    saveAll(categoriaLancamentoFinanceiroList: CategoriaLancamentoFinanceiroModel[]): Promise<CategoriaLancamentoFinanceiroModel[]>;
    findAll(): Promise<(import("mongoose").Document<any, any, CategoriaLancamentoFinanceiroModel> & CategoriaLancamentoFinanceiroModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
