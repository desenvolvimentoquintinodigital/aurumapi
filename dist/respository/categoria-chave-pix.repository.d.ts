import { Model } from "mongoose";
import { CategoriaChavePixModel } from "src/model/categoria-chave-pix.model";
export declare class CategoriaChavePIXRepository {
    private readonly categoriaChavePIXRepository;
    constructor(categoriaChavePIXRepository: Model<CategoriaChavePixModel>);
    saveOne(categoriaChavePixModel: CategoriaChavePixModel): Promise<import("mongoose").Document<any, any, CategoriaChavePixModel> & CategoriaChavePixModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    saveAll(categoriaChavePixModelList: CategoriaChavePixModel[]): Promise<CategoriaChavePixModel[]>;
    findAll(): Promise<(import("mongoose").Document<any, any, CategoriaChavePixModel> & CategoriaChavePixModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
