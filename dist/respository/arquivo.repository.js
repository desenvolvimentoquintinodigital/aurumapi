"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArquivoRepository = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const arquivo_model_1 = require("../model/arquivo.model");
let ArquivoRepository = class ArquivoRepository {
    constructor(arquivoRepository) {
        this.arquivoRepository = arquivoRepository;
    }
    async saveOne(arquivoModel) {
        return await new this.arquivoRepository(arquivoModel).save();
    }
    async findAll() {
        return await this.arquivoRepository.find({}).lean().exec();
    }
    async findOne(arquivoID) {
        return (await this.arquivoRepository.findById(arquivoID)).conteudo;
    }
    async deleteOne(arquivoID) {
        return await this.arquivoRepository.findByIdAndDelete(arquivoID);
    }
};
ArquivoRepository = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)(arquivo_model_1.ArquivoModel.name)),
    __metadata("design:paramtypes", [mongoose_2.Model])
], ArquivoRepository);
exports.ArquivoRepository = ArquivoRepository;
//# sourceMappingURL=arquivo.repository.js.map