import { Model } from "mongoose";
import { CategoriaFormaPagamentoModel } from './../model/categoria-forma-pagamento.model';
export declare class CategoriaFormaPagamentoRepository {
    private readonly formaPagamentoRepository;
    constructor(formaPagamentoRepository: Model<CategoriaFormaPagamentoModel>);
    saveOne(categoriaFormaPagamentoModel: CategoriaFormaPagamentoModel): Promise<import("mongoose").Document<any, any, CategoriaFormaPagamentoModel> & CategoriaFormaPagamentoModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    findAll(): Promise<(import("mongoose").Document<any, any, CategoriaFormaPagamentoModel> & CategoriaFormaPagamentoModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
