"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChavePIXRepository = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const chave_pix_model_1 = require("./../model/chave-pix.model");
let ChavePIXRepository = class ChavePIXRepository {
    constructor(chavePixRepository) {
        this.chavePixRepository = chavePixRepository;
    }
    async saveOne(chavePixModel) {
        return await new this.chavePixRepository(chavePixModel).save();
    }
    async saveAll(chavePixModelList) {
        for (let categoriaLancamentoFinanceiroModel of chavePixModelList) {
            new this.chavePixRepository(categoriaLancamentoFinanceiroModel).save();
        }
        return chavePixModelList;
    }
    async findAll() {
        return await this.chavePixRepository.find().exec();
    }
};
ChavePIXRepository = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)(chave_pix_model_1.ChavePixModel.name)),
    __metadata("design:paramtypes", [mongoose_2.Model])
], ChavePIXRepository);
exports.ChavePIXRepository = ChavePIXRepository;
//# sourceMappingURL=chave-pix.repository.js.map