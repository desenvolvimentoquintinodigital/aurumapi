"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LancamentoFinanceiroModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const lancamento_financeiro_controller_1 = require("./../controller/lancamento-financeiro.controller");
const lancamento_financeiro_model_1 = require("./../model/lancamento-financeiro.model");
const lancamento_financeiro_repository_1 = require("./../respository/lancamento-financeiro.repository");
const lancamento_financeiro_service_1 = require("./../service/lancamento-financeiro.service");
let LancamentoFinanceiroModule = class LancamentoFinanceiroModule {
};
LancamentoFinanceiroModule = __decorate([
    (0, common_1.Module)({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: lancamento_financeiro_model_1.LancamentoFinanceiroModel.name, schema: lancamento_financeiro_model_1.LancamentoFinanceiroSchema }])],
        controllers: [lancamento_financeiro_controller_1.LancamentoFinanceiroController],
        providers: [
            lancamento_financeiro_service_1.LancamentoFinanceiroService,
            lancamento_financeiro_repository_1.LancamentoFinanceiroRepository
        ]
    })
], LancamentoFinanceiroModule);
exports.LancamentoFinanceiroModule = LancamentoFinanceiroModule;
//# sourceMappingURL=lancamento-financeiro.module.js.map