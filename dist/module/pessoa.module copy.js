"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PessoaModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const pessoa_model_1 = require("../model/pessoa.model");
const pessoa_controller_1 = require("../controller/pessoa.controller");
const pessoa_service_1 = require("../service/pessoa.service");
const pessoa_repository_1 = require("../respository/pessoa.repository");
let PessoaModule = class PessoaModule {
};
PessoaModule = __decorate([
    (0, common_1.Module)({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: pessoa_model_1.PessoaModel.name, schema: pessoa_model_1.PessoaSchema }])],
        controllers: [pessoa_controller_1.PessoaController],
        providers: [
            pessoa_service_1.PessoaService,
            pessoa_repository_1.PessoaRepository
        ]
    })
], PessoaModule);
exports.PessoaModule = PessoaModule;
//# sourceMappingURL=pessoa.module%20copy.js.map