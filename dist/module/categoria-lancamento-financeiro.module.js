"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoriaLancamentoFinanceiroModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const categoria_lancamento_financeiro_controller_1 = require("./../controller/categoria-lancamento-financeiro.controller");
const categoria_lancamento_financeiro_model_1 = require("./../model/categoria-lancamento-financeiro.model");
const categoria_lancamento_financeiro_repository_1 = require("./../respository/categoria-lancamento-financeiro.repository");
const categoria_lancamento_financeiro_service_1 = require("./../service/categoria-lancamento-financeiro.service");
let CategoriaLancamentoFinanceiroModule = class CategoriaLancamentoFinanceiroModule {
};
CategoriaLancamentoFinanceiroModule = __decorate([
    (0, common_1.Module)({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: categoria_lancamento_financeiro_model_1.CategoriaLancamentoFinanceiroModel.name, schema: categoria_lancamento_financeiro_model_1.CategoriaLancamentoFinanceiroSchema }])],
        controllers: [categoria_lancamento_financeiro_controller_1.CategoriaLancamentoFinanceiroController],
        providers: [
            categoria_lancamento_financeiro_service_1.CategoriaLancamentoFinanceiroService,
            categoria_lancamento_financeiro_repository_1.CategoriaLancamentoFinanceiroRepository
        ]
    })
], CategoriaLancamentoFinanceiroModule);
exports.CategoriaLancamentoFinanceiroModule = CategoriaLancamentoFinanceiroModule;
//# sourceMappingURL=categoria-lancamento-financeiro.module.js.map