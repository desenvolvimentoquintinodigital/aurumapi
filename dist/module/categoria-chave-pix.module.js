"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoriaChavePIXModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const categoria_chave_pix_model_1 = require("../model/categoria-chave-pix.model");
const categoria_chave_pix_controller_1 = require("../controller/categoria-chave-pix.controller");
const categoria_chave_pix_repository_1 = require("./../respository/categoria-chave-pix.repository");
const categoria_chave_pix_service_1 = require("./../service/categoria-chave-pix.service");
let CategoriaChavePIXModule = class CategoriaChavePIXModule {
};
CategoriaChavePIXModule = __decorate([
    (0, common_1.Module)({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: categoria_chave_pix_model_1.CategoriaChavePixModel.name, schema: categoria_chave_pix_model_1.CategoriaChavePixSchema }])],
        controllers: [categoria_chave_pix_controller_1.CategoriaChavePIXController],
        providers: [
            categoria_chave_pix_service_1.CategoriaChavePIXService,
            categoria_chave_pix_repository_1.CategoriaChavePIXRepository
        ]
    })
], CategoriaChavePIXModule);
exports.CategoriaChavePIXModule = CategoriaChavePIXModule;
//# sourceMappingURL=categoria-chave-pix.module.js.map