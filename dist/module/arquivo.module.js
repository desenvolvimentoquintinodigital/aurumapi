"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArquivoModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const arquivo_controller_1 = require("./../controller/arquivo.controller");
const arquivo_model_1 = require("./../model/arquivo.model");
const arquivo_repository_1 = require("./../respository/arquivo.repository");
const arquivo_service_1 = require("./../service/arquivo.service");
let ArquivoModule = class ArquivoModule {
};
ArquivoModule = __decorate([
    (0, common_1.Module)({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: arquivo_model_1.ArquivoModel.name, schema: arquivo_model_1.ArquivoSchema }])],
        controllers: [arquivo_controller_1.ArquivoController],
        providers: [
            arquivo_service_1.ArquivoService,
            arquivo_repository_1.ArquivoRepository
        ]
    })
], ArquivoModule);
exports.ArquivoModule = ArquivoModule;
//# sourceMappingURL=arquivo.module.js.map