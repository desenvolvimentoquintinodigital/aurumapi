"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CategoriaFormaPagamentoModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoriaFormaPagamentoModule = void 0;
const categoria_forma_pagamento_model_1 = require("./../model/categoria-forma-pagamento.model");
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const categoria_pessoa_controller_1 = require("../controller/categoria-pessoa.controller");
const categoria_pessoa_service_1 = require("../service/categoria-pessoa.service");
const categoria_pessoa_repository_1 = require("../respository/categoria-pessoa.repository");
let CategoriaFormaPagamentoModule = CategoriaFormaPagamentoModule_1 = class CategoriaFormaPagamentoModule {
};
CategoriaFormaPagamentoModule = CategoriaFormaPagamentoModule_1 = __decorate([
    (0, common_1.Module)({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: CategoriaFormaPagamentoModule_1.name, schema: categoria_forma_pagamento_model_1.CategoriaFormaPagamentoSchema }])],
        controllers: [categoria_pessoa_controller_1.CategoriaPessoaController],
        providers: [
            categoria_pessoa_service_1.CategoriaPessoaService,
            categoria_pessoa_repository_1.CategoriaPessoaRepository
        ]
    })
], CategoriaFormaPagamentoModule);
exports.CategoriaFormaPagamentoModule = CategoriaFormaPagamentoModule;
//# sourceMappingURL=forma-pagamento.module.js.map