"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChavePixModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const chave_pix_controller_1 = require("../controller/chave-pix.controller");
const chave_pix_model_1 = require("./../model/chave-pix.model");
const chave_pix_repository_1 = require("./../respository/chave-pix.repository");
const chave_pix_service_1 = require("./../service/chave-pix.service");
let ChavePixModule = class ChavePixModule {
};
ChavePixModule = __decorate([
    (0, common_1.Module)({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: chave_pix_model_1.ChavePixModel.name, schema: chave_pix_model_1.ChavePixSchema }])],
        controllers: [chave_pix_controller_1.ChavePixController],
        providers: [
            chave_pix_service_1.ChavePixService,
            chave_pix_repository_1.ChavePIXRepository
        ]
    })
], ChavePixModule);
exports.ChavePixModule = ChavePixModule;
//# sourceMappingURL=chave-pix.module.js.map