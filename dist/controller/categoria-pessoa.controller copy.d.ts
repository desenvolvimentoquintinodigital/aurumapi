/// <reference types="mongoose" />
import { CategoriaPessoaService } from "../service/categoria-pessoa.service";
import { CategoriaPessoaModel } from "../model/categoria-pessoa.model";
export declare class CategoriaPessoaController {
    private readonly categoriaPessoaService;
    constructor(categoriaPessoaService: CategoriaPessoaService);
    saveOne(categoriaPessoaModel: CategoriaPessoaModel): Promise<import("mongoose").Document<any, any, CategoriaPessoaModel> & CategoriaPessoaModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    findAll(): Promise<(import("mongoose").Document<any, any, CategoriaPessoaModel> & CategoriaPessoaModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
