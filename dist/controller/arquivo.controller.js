"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArquivoController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const arquivo_service_1 = require("./../service/arquivo.service");
let ArquivoController = class ArquivoController {
    constructor(arquivoService) {
        this.arquivoService = arquivoService;
    }
    async saveOne(file) {
        return this.arquivoService.saveOne(file);
    }
    async findAll() {
        return this.arquivoService.findAll();
    }
    async findOne(arquivoID) {
        return this.arquivoService.findOne(arquivoID);
    }
};
__decorate([
    (0, common_1.Post)("upload"),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('file')),
    __param(0, (0, common_1.UploadedFile)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArquivoController.prototype, "saveOne", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ArquivoController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(":arquivoID"),
    __param(0, (0, common_1.Param)("arquivoID")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ArquivoController.prototype, "findOne", null);
ArquivoController = __decorate([
    (0, common_1.Controller)('arquivo'),
    __metadata("design:paramtypes", [arquivo_service_1.ArquivoService])
], ArquivoController);
exports.ArquivoController = ArquivoController;
//# sourceMappingURL=arquivo.controller.js.map