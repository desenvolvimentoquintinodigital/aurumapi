/// <reference types="mongoose" />
import { PessoaService } from "../service/pessoa.service";
import { PessoaModel } from "../model/pessoa.model";
export declare class PessoaController {
    private readonly pessoaService;
    constructor(pessoaService: PessoaService);
    saveOne(pessoaModel: PessoaModel): Promise<import("mongoose").Document<any, any, PessoaModel> & PessoaModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    findAll(): Promise<(import("mongoose").Document<any, any, PessoaModel> & PessoaModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
