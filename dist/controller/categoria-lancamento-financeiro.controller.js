"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoriaLancamentoFinanceiroController = void 0;
const common_1 = require("@nestjs/common");
const categoria_lancamento_financeiro_model_1 = require("./../model/categoria-lancamento-financeiro.model");
const categoria_lancamento_financeiro_service_1 = require("./../service/categoria-lancamento-financeiro.service");
let CategoriaLancamentoFinanceiroController = class CategoriaLancamentoFinanceiroController {
    constructor(categoriaLancamentoFinanceiro) {
        this.categoriaLancamentoFinanceiro = categoriaLancamentoFinanceiro;
    }
    async saveOne(categoriaLancamentoFinanceiroModel) {
        return this.categoriaLancamentoFinanceiro.saveOne(categoriaLancamentoFinanceiroModel);
    }
    async saveAll(categoriaLancamentoFinanceiroList) {
        return await this.categoriaLancamentoFinanceiro.saveAll(categoriaLancamentoFinanceiroList);
    }
    async findAll() {
        return await this.categoriaLancamentoFinanceiro.findAll();
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [categoria_lancamento_financeiro_model_1.CategoriaLancamentoFinanceiroModel]),
    __metadata("design:returntype", Promise)
], CategoriaLancamentoFinanceiroController.prototype, "saveOne", null);
__decorate([
    (0, common_1.Post)("/all"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], CategoriaLancamentoFinanceiroController.prototype, "saveAll", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CategoriaLancamentoFinanceiroController.prototype, "findAll", null);
CategoriaLancamentoFinanceiroController = __decorate([
    (0, common_1.Controller)('categoria-lancamento-financeiro'),
    __metadata("design:paramtypes", [categoria_lancamento_financeiro_service_1.CategoriaLancamentoFinanceiroService])
], CategoriaLancamentoFinanceiroController);
exports.CategoriaLancamentoFinanceiroController = CategoriaLancamentoFinanceiroController;
//# sourceMappingURL=categoria-lancamento-financeiro.controller.js.map