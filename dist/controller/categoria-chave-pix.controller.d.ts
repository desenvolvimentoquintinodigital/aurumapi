/// <reference types="mongoose" />
import { CategoriaChavePixModel } from 'src/model/categoria-chave-pix.model';
import { CategoriaChavePIXService } from './../service/categoria-chave-pix.service';
export declare class CategoriaChavePIXController {
    private readonly categoriaChavePIXService;
    constructor(categoriaChavePIXService: CategoriaChavePIXService);
    saveOne(categoriaChavePixModel: CategoriaChavePixModel): Promise<import("mongoose").Document<any, any, CategoriaChavePixModel> & CategoriaChavePixModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    saveAll(categoriaChavePixModel: CategoriaChavePixModel[]): Promise<CategoriaChavePixModel[]>;
    findAll(): Promise<(import("mongoose").Document<any, any, CategoriaChavePixModel> & CategoriaChavePixModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
