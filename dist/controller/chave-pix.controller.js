"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChavePixController = void 0;
const common_1 = require("@nestjs/common");
const chave_pix_model_1 = require("./../model/chave-pix.model");
const chave_pix_service_1 = require("./../service/chave-pix.service");
let ChavePixController = class ChavePixController {
    constructor(chavePixService) {
        this.chavePixService = chavePixService;
    }
    async saveOne(chavePixModel) {
        return this.chavePixService.saveOne(chavePixModel);
    }
    async saveAll(chavePixModel) {
        return await this.chavePixService.saveAll(chavePixModel);
    }
    async findAll() {
        return await this.chavePixService.findAll();
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [chave_pix_model_1.ChavePixModel]),
    __metadata("design:returntype", Promise)
], ChavePixController.prototype, "saveOne", null);
__decorate([
    (0, common_1.Post)("/all"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], ChavePixController.prototype, "saveAll", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ChavePixController.prototype, "findAll", null);
ChavePixController = __decorate([
    (0, common_1.Controller)('chave-pix'),
    __metadata("design:paramtypes", [chave_pix_service_1.ChavePixService])
], ChavePixController);
exports.ChavePixController = ChavePixController;
//# sourceMappingURL=chave-pix.controller.js.map