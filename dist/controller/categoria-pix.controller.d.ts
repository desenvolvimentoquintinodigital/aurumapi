import { CategoriaChavePixModel } from 'src/model/categoria-chave-pix.model';
import { CategoriaPIXService } from './../service/categoria-pix.service';
export declare class CategoriaPIXController {
    private readonly categoriaPIXService;
    constructor(categoriaPIXService: CategoriaPIXService);
    saveOne(categoriaChavePixModel: CategoriaChavePixModel): Promise<any>;
    saveAll(categoriaChavePixModel: CategoriaChavePixModel[]): Promise<any>;
    findAll(): Promise<any>;
}
