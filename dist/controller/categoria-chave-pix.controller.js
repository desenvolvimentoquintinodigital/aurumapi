"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoriaChavePIXController = void 0;
const common_1 = require("@nestjs/common");
const categoria_chave_pix_model_1 = require("../model/categoria-chave-pix.model");
const categoria_chave_pix_service_1 = require("./../service/categoria-chave-pix.service");
let CategoriaChavePIXController = class CategoriaChavePIXController {
    constructor(categoriaChavePIXService) {
        this.categoriaChavePIXService = categoriaChavePIXService;
    }
    async saveOne(categoriaChavePixModel) {
        console.log(categoriaChavePixModel);
        return this.categoriaChavePIXService.saveOne(categoriaChavePixModel);
    }
    async saveAll(categoriaChavePixModel) {
        return await this.categoriaChavePIXService.saveAll(categoriaChavePixModel);
    }
    async findAll() {
        return await this.categoriaChavePIXService.findAll();
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [categoria_chave_pix_model_1.CategoriaChavePixModel]),
    __metadata("design:returntype", Promise)
], CategoriaChavePIXController.prototype, "saveOne", null);
__decorate([
    (0, common_1.Post)("/all"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], CategoriaChavePIXController.prototype, "saveAll", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CategoriaChavePIXController.prototype, "findAll", null);
CategoriaChavePIXController = __decorate([
    (0, common_1.Controller)('categoria-chave-pix'),
    __metadata("design:paramtypes", [categoria_chave_pix_service_1.CategoriaChavePIXService])
], CategoriaChavePIXController);
exports.CategoriaChavePIXController = CategoriaChavePIXController;
//# sourceMappingURL=categoria-chave-pix.controller.js.map