/// <reference types="mongoose" />
import { CategoriaFormaPagamentoModel } from 'src/model/categoria-forma-pagamento.model';
import { CategoriaFormaPagamentoService } from './../service/categoria-forma-pagamento.service';
export declare class CategoriaFormaPagamentoController {
    private readonly categoriaFormaPagamentoService;
    constructor(categoriaFormaPagamentoService: CategoriaFormaPagamentoService);
    saveOne(categoriaFormaPagamentoModel: CategoriaFormaPagamentoModel): Promise<import("mongoose").Document<any, any, CategoriaFormaPagamentoModel> & CategoriaFormaPagamentoModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    findAll(): Promise<(import("mongoose").Document<any, any, CategoriaFormaPagamentoModel> & CategoriaFormaPagamentoModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
