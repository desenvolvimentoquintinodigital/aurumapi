/// <reference types="mongoose" />
import { CategoriaLancamentoFinanceiroModel } from './../model/categoria-lancamento-financeiro.model';
import { CategoriaLancamentoFinanceiroService } from './../service/categoria-lancamento-financeiro.service';
export declare class CategoriaLancamentoFinanceiroController {
    private readonly categoriaLancamentoFinanceiro;
    constructor(categoriaLancamentoFinanceiro: CategoriaLancamentoFinanceiroService);
    saveOne(categoriaLancamentoFinanceiroModel: CategoriaLancamentoFinanceiroModel): Promise<import("mongoose").Document<any, any, CategoriaLancamentoFinanceiroModel> & CategoriaLancamentoFinanceiroModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    saveAll(categoriaLancamentoFinanceiroList: CategoriaLancamentoFinanceiroModel[]): Promise<CategoriaLancamentoFinanceiroModel[]>;
    findAll(): Promise<(import("mongoose").Document<any, any, CategoriaLancamentoFinanceiroModel> & CategoriaLancamentoFinanceiroModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
