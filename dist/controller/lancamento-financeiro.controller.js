"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LancamentoFinanceiroController = void 0;
const common_1 = require("@nestjs/common");
const lancamento_financeiro_model_1 = require("../model/lancamento-financeiro.model");
const lancamento_financeiro_service_1 = require("./../service/lancamento-financeiro.service");
let LancamentoFinanceiroController = class LancamentoFinanceiroController {
    constructor(lancamentoFinanceiroService) {
        this.lancamentoFinanceiroService = lancamentoFinanceiroService;
    }
    async saveOne(lancamentoFinanceiroModel) {
        console.log(lancamentoFinanceiroModel);
        return this.lancamentoFinanceiroService.saveOne(lancamentoFinanceiroModel);
    }
    async saveAll(lancamentoFinanceiroModel) {
        return await this.lancamentoFinanceiroService.saveAll(lancamentoFinanceiroModel);
    }
    async findAll() {
        return await this.lancamentoFinanceiroService.findAll();
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [lancamento_financeiro_model_1.LancamentoFinanceiroModel]),
    __metadata("design:returntype", Promise)
], LancamentoFinanceiroController.prototype, "saveOne", null);
__decorate([
    (0, common_1.Post)("/all"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Array]),
    __metadata("design:returntype", Promise)
], LancamentoFinanceiroController.prototype, "saveAll", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], LancamentoFinanceiroController.prototype, "findAll", null);
LancamentoFinanceiroController = __decorate([
    (0, common_1.Controller)('lancamento-financeiro'),
    __metadata("design:paramtypes", [lancamento_financeiro_service_1.LancamentoFinanceiroService])
], LancamentoFinanceiroController);
exports.LancamentoFinanceiroController = LancamentoFinanceiroController;
//# sourceMappingURL=lancamento-financeiro.controller.js.map