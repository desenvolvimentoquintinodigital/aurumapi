/// <reference types="mongoose" />
import { ChavePixModel } from './../model/chave-pix.model';
import { ChavePixService } from './../service/chave-pix.service';
export declare class ChavePixController {
    private readonly chavePixService;
    constructor(chavePixService: ChavePixService);
    saveOne(chavePixModel: ChavePixModel): Promise<import("mongoose").Document<any, any, ChavePixModel> & ChavePixModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    saveAll(chavePixModel: ChavePixModel[]): Promise<ChavePixModel[]>;
    findAll(): Promise<(import("mongoose").Document<any, any, ChavePixModel> & ChavePixModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
