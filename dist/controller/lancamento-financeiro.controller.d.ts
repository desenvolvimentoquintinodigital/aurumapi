/// <reference types="mongoose" />
import { LancamentoFinanceiroModel } from 'src/model/lancamento-financeiro.model';
import { LancamentoFinanceiroService } from './../service/lancamento-financeiro.service';
export declare class LancamentoFinanceiroController {
    private readonly lancamentoFinanceiroService;
    constructor(lancamentoFinanceiroService: LancamentoFinanceiroService);
    saveOne(lancamentoFinanceiroModel: LancamentoFinanceiroModel): Promise<import("mongoose").Document<any, any, LancamentoFinanceiroModel> & LancamentoFinanceiroModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    saveAll(lancamentoFinanceiroModel: LancamentoFinanceiroModel[]): Promise<LancamentoFinanceiroModel[]>;
    findAll(): Promise<(import("mongoose").Document<any, any, LancamentoFinanceiroModel> & LancamentoFinanceiroModel & {
        _id: import("mongoose").Types.ObjectId;
    })[]>;
}
