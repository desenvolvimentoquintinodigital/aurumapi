"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoriaFormaPagamentoController = void 0;
const common_1 = require("@nestjs/common");
const categoria_forma_pagamento_model_1 = require("../model/categoria-forma-pagamento.model");
const categoria_forma_pagamento_service_1 = require("./../service/categoria-forma-pagamento.service");
let CategoriaFormaPagamentoController = class CategoriaFormaPagamentoController {
    constructor(categoriaFormaPagamentoService) {
        this.categoriaFormaPagamentoService = categoriaFormaPagamentoService;
    }
    async saveOne(categoriaFormaPagamentoModel) {
        return this.categoriaFormaPagamentoService.saveOne(categoriaFormaPagamentoModel);
    }
    async findAll() {
        return await this.categoriaFormaPagamentoService.findAll();
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [categoria_forma_pagamento_model_1.CategoriaFormaPagamentoModel]),
    __metadata("design:returntype", Promise)
], CategoriaFormaPagamentoController.prototype, "saveOne", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CategoriaFormaPagamentoController.prototype, "findAll", null);
CategoriaFormaPagamentoController = __decorate([
    (0, common_1.Controller)('categoria-forma-pagamento'),
    __metadata("design:paramtypes", [categoria_forma_pagamento_service_1.CategoriaFormaPagamentoService])
], CategoriaFormaPagamentoController);
exports.CategoriaFormaPagamentoController = CategoriaFormaPagamentoController;
//# sourceMappingURL=categoria-forma-pagamento.controller.js.map