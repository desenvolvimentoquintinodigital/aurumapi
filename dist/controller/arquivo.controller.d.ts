/// <reference types="multer" />
/// <reference types="node" />
import { ArquivoModel } from 'src/model/arquivo.model';
import { ArquivoService } from './../service/arquivo.service';
export declare class ArquivoController {
    private readonly arquivoService;
    constructor(arquivoService: ArquivoService);
    saveOne(file: Express.Multer.File): Promise<{
        message: string;
        urlArquivo: string;
    }>;
    findAll(): Promise<(ArquivoModel & {
        _id: import("mongoose").Schema.Types.ObjectId;
    })[]>;
    findOne(arquivoID: any): Promise<Buffer>;
}
